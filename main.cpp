#include <iostream>
#include <vector>
#include <algorithm>
#include <cassert>
#include <queue>


using namespace std;

const int capacity = 4;

template<typename T>
class node
{
public:
    node()
    {
        parent = nullptr;
    }

    const T& firstKey()
    {
        return keys.front();
    }

    void addKey(const T& k)
    {
        auto it = lower_bound(keys.begin(), keys.end(), k);
        if(it != keys.end() && *it == k)
            return;
        keys.insert(it, k);
    }
    void removeKey(const T& k)
    {
        auto it = lower_bound(keys.begin(), keys.end(), k);
        if(*it != k)
        {
            cout << "wut";
        }
        keys.erase(it);
    }

    void addChild(node* n)
    {
        auto it = lower_bound(child.begin(), child.end(), n, [](node* a, node* b){return a->firstKey() < b->firstKey();});
        if(it != child.end() && *it == n)
            return;
        child.insert(it, n);
        n->parent = this;
    }
    void removeChild(node* n)
    {
        auto it = find(child.begin(), child.end(), n);
        child.erase(it);
        n->parent = nullptr;
    }

    node* getParent()
    {
        return parent;
    }

    bool isLeaf()
    {
        return (child.size() == 0);
    }

    bool overflowed()
    {
        return (keys.size() >= capacity) || (child.size() > capacity);
    }

    bool underflowed()
    {
        if(parent == nullptr)
            return false;

        if(isLeaf())
            return (keys.size() <= capacity/2);
        return (keys.size() < (capacity/2 - 1));
    }

    int height()
    {
        int jumps=0;
        node* atual = parent;
        while(atual != nullptr)
        {
            jumps++;
            atual = atual->parent;
        }
        return jumps;
    }

    void print()
    {
        for(const T &atual : keys)
            cout << atual << " ";
        cout << " | ";
    }

    //não sendo um nó folha, retorna um ponteiro ao próximo nó que deve ser visitado na busca pela chave k
    node<T>* nextNode(const T& k)
    {
        auto it = lower_bound(keys.begin(), keys.end(), k);
        if(it != keys.end() && *it == k)
            it++;
        return child[it - keys.begin()];
    }

    //faz uma operação split, retornando o ponteiro ao novo nó criado. O nó original tem seu tamanho reduzido. O nó retornado não tem pai definido.
    node<T>* split()
    {
        assert(keys.size() >= capacity);
        node<T>* novo = new node<T>;

        //copia parte dos elementos para o novo nó, de forma a resolver o overflow.
        for(int i=keys.size()/2; i < keys.size(); i++)
            novo->addKey(keys[i]);
        for(int i=keys.size()/2 + 1; i < child.size(); i++)
            novo->addChild(child[i]);

        size_t size = keys.size()/2;
        keys.resize(size);
        if(child.size() > 0) child.resize(size + 1);

        return novo;
    }

    void merge(node* outro)
    {
        assert(outro->parent == parent);

        for(const T& k : outro->keys)
            addKey(k);
        for(node* n : outro->child)
            addChild(n);

        parent->removeChild(outro);
        delete outro;
    }

    const std::vector<node*>& getChildren()
    {
        return child;
    }

    const std::vector<T>& getKeys()
    {
        return keys;
    }

    node* getSibling(T* elemento_meio)
    {
        auto it = find(parent->child.begin(), parent->child.end(), this);
        int indice = it - parent->child.begin();

        if(indice == 0)
        {
            *elemento_meio = parent->keys[0];
            return parent->child[1];
        }

        *elemento_meio = parent->keys[indice - 1];
        return parent->child[indice - 1];

    }

private:
    node* parent;
    std::vector<T> keys;
    std::vector<node*> child;
};


template<typename T>
class BPlusTree
{
public:
    BPlusTree()
    {
        root = new node<T>;
    }
    ~BPlusTree()
    {
        delete root;
    }

    void addKey(const T& k)
    {
        node<T>* leaf = findLeaf(k);
        leaf->addKey(k);
        fix_overflow(leaf);
    }

    void removeKey(const T& k)
    {
        node<T>* leaf = findLeaf(k);
        leaf->removeKey(k);
        fix_underflow(leaf);
    }

    bool exists(const T& k)
    {
        node<T>* leaf = findLeaf(k);
        auto it = lower_bound(leaf->getKeys().begin(), leaf->getKeys().end(), k);

        if(it == leaf->getKeys().end())
            return false;

        return (*it == k);
    }

    void print()
    {
        int last_height = 0;
        queue<node<T>*> fila;
        fila.push(root);

        while(!fila.empty())
        {
            node<T>* atual = fila.front();
            fila.pop();

            if(last_height != atual->height())
                cout << "\n";

            atual->print();

            for(auto &a: atual->getChildren())
                fila.push(a);

            last_height = atual->height();
        }
        cout << "\n----------------\n";
    }

private:
    void fix_overflow(node<T>* atual)
    {
        if(atual->overflowed())
        {
            node<T>* novo = atual->split();
            T meio = novo->firstKey();
            if(atual->getParent() == nullptr)
            {
                node<T>* nova_root = new node<T>;
                nova_root->addChild(atual);
                root = nova_root;
            }
            if(!atual->isLeaf())
                novo->removeKey(novo->firstKey());
            atual->getParent()->addKey(meio);
            atual->getParent()->addChild(novo);
            fix_overflow(atual->getParent());
        }
    }

    void fix_underflow(node<T>* atual)
    {
        if(atual == nullptr)
            return;
        if(atual->underflowed())
        {
            T chave_meio;
            node<T>* vizinho = atual->getSibling(&chave_meio);
            atual->getParent()->removeKey(chave_meio);
            if(!atual->isLeaf())
                atual->addKey(chave_meio); //se não for uma folha, o elemento que separa os nós participará do merge
            atual->merge(vizinho);

            if(atual->overflowed()) //merge resultou num nó com chaves demais
            {
                node<T>* novo = atual->split();
                atual->getParent()->addKey(novo->firstKey());
                if(!atual->isLeaf())
                    novo->removeKey(novo->firstKey());
                atual->getParent()->addChild(novo);
            }

            if((atual->getParent() == root) && (root->getChildren().size() == 1))
            {
                root->removeChild(atual);
                delete root;
                root = atual;
            }

            fix_underflow(atual->getParent());
        }
    }


    node<T>* findLeaf(const T& k)
    {
        node<T>* atual = root;

        while(!atual->isLeaf())
            atual = atual->nextNode(k);

        return atual;
    }

    node<T>* root;

};
#include <ctime>
#include <set>

int teste()
{
    cout << "outro teste -------------\n";
    BPlusTree<int> arv;
    std::set<int> testdata; //inteiros que devem estar na árvore

    for(int i=0; i < 2000; i++)
    {
        int n = rand()%30000;

        while(testdata.count(n) == 1) //garante um inteiro único
            n = rand()%30000;

        arv.addKey(n);
        testdata.insert(n);
    }


    while(testdata.size() > 0)
    {
        //cout << "comecando: " << endl;
        //arv.print();

        for(int i : testdata) //testa se vai achar todos os elementos
        {
            if(!arv.exists(i))
            {
                cout << "ERRO ao encontrar " << i << endl;
                return 1;
            }
        }

        //remove um aleatorio
        int indice = rand() % testdata.size();
        auto it = std::next(testdata.begin(), indice);
        arv.removeKey(*it);

        //cout << "Removio " << *it << endl;
        testdata.erase(it);

        //cout << "--------------------------------------------" << endl;

    }

    cout << "teste ok\n";
    return 0;
}
#include <ctime>

int main()
{
    srand(time(NULL));

    clock_t start, end;
    double cpu_time_used;

    start = clock();


    for(int i=0; i < 200; i++)
    {
        cout << "Teste " << i << endl;
        teste();
    }

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    cout <<  cpu_time_used << " segundos.\n";
    return 0;
}
