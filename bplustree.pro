#-------------------------------------------------
#
# Project created by QtCreator 2017-01-05T13:36:22
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = bplustree
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app
CONFIG += c++11


SOURCES += main.cpp
